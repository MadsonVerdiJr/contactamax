# Contacta Max - Prova

Stack Versions:

- PHP: 7.2
- Mysql: 8.0

## Installation

This system was developed using the basics of laravel requirements, more informations can be found at [Laravel Official Documentation](https://laravel.com/docs/5.6/installation#installation).

For provisioning a virtual ambient for development and tests I used [Laradock](http://laradock.io), a nice to have tool for docker containers. If Laradock is used in more than one project you need to create a new database, this can be done using [this code](#create-mysql-database-and-grant-permissions).

### Create MySQL database and Grant permissions

```
CREATE USER 'contacta'@'%' IDENTIFIED BY 'max';

CREATE DATABASE IF NOT EXISTS `contactamax` COLLATE 'utf8_general_ci' ;
GRANT ALL ON `contactamax`.* TO 'contacta'@'%';
```

To prevent an issue on Laravel authentication driver, for **MySQL 8+**, use:

```
CREATE USER 'contacta'@'%' IDENTIFIED WITH mysql_native_password BY 'max';

CREATE DATABASE IF NOT EXISTS `contactamax` COLLATE 'utf8_general_ci' ;
GRANT ALL ON `contactamax`.* TO 'contacta'@'%';
```

## System overview

This is an overview of the system.

All system is protect by authentication using middlewares, and was created using artisan command (`php artisan make:auth`). The API uses Auth Basic for more practical use of Postman.


![ER Diagram](https://preview.ibb.co/j0Ac18/graph.png)

The ER diagram was generated using [laravel-er-diagram-generator](https://github.com/beyondcode/laravel-er-diagram-generator).

A Postman collection is available at the root of the repository as `api.postman_collection.json`.

[Version Roadmap](#version-roadmap) describe the versions created for this system.

### Version Roadmap

- v0.1.0
    + Fork from [Laravel boilerplate](https://bitbucket.org/rosulonline/laravel/)
- v0.2.0
    + CRUD for products (with SKU)
- v0.2.1
    + Screen for Add quantity to some product
- v0.2.2
    + Screen for Remove quantity of some product
- v0.3.0
    + API for Add/Remove quantity of some product
        * `/api/baixar-produtos/`
        * `/api/adicionar-produtos/`
- v0.4.0
    + Report daily product movement
        * How many and which products were added
        * How many and which products were removed
        * This operation was made by interface or API
        * Alert for a product with 100 units in stock
- v0.5.0
    + Protect with User/password
- v0.5.1
    + Documentation for README.md

