<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * Mass assignment fields
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'quantity',
        'sku'
    ];

    /**
     * Scope a query to only include low quantity.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeLowStock($query)
    {
        return $query->where('quantity', '<', 100);
    }

    public function stock()
    {
        return $this->hasMany(ProductStock::class, 'product_id', 'id');
    }
}
