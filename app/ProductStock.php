<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductStock extends Model
{
    /**
     * Mass assignment
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'type',
        'method',
        'quantity'
    ];

    /**
     * Define an Accessor for tiny int var
     *
     * @param  integer $value
     *
     * @return string
     */
    public function getMethodAttribute($value)
    {
        return $value == 0 ? 'Sistema' : 'API';
    }

    /**
     * Define an Accessor for the tinyint var
     * 0 - in
     * 1 - out
     *
     * @param  integer $value
     *
     * @return string
     */
    public function getTypeAttribute($value)
    {
        // retun a string for better manipulation
        return $value == 0 ? 'in' : 'out' ;
    }

    /**
     * Return the product of the stock operation
     *
     * @return Relationship
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    /**
     * Scope for filter daily content
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \Carbon\Carbon                        $date
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDaily($query, \Carbon\Carbon $date)
    {
        $start = $date->copy()->startOfDay();
        $end = $date->copy()->endOfDay();

        return $query->whereBetween('created_at', [$start, $end]);
    }

    /**
     * Scope for in stocks
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTypeIn($query)
    {
        return $query->whereType(0);
    }

    /**
     * Scope for in stocks
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTypeOut($query)
    {
        return $query->whereType(1);
    }

    /**
     * Define an Mutator for var
     *
     * @param string $value
     */
    public function setMethodAttribute($value)
    {
        $this->attributes['method'] = $value == 'Sistema' ? 0 : 1;
    }

    /**
     * Define an Mutator for var
     *
     * @param string $value
     */
    public function setTypeAttribute($value)
    {
        $this->attributes['type'] = $value == 'in' ? 0 : 1;
    }
}
