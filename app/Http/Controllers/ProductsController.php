<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductStoreRequest;
use App\Repositories\ProductRepositoryInterface;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Product Repository Interface
     *
     * @var App\Repositories\ProductRepositoryInterface
     */
    private $productInterface;

    /**
     * Construct class
     *
     * @param ProductRepositoryInterface $productInterface - Product Repository Interface
     */
    public function __construct(ProductRepositoryInterface $productInterface)
    {
        $this->middleware('auth');
        $this->productInterface = $productInterface;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = $this->productInterface->getAllPaginated();

        return view('products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(ProductStoreRequest $request)
    {
        $product = $this->productInterface->store($request->name, $request->quantity);

        return redirect()
            ->route('produtos.show', $product->sku)
            ->with('status.success', 'Produto criado com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $product = $this->productInterface->getBySku($request->produto);

        return view('products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $product = $this->productInterface->getBySku($request->produto);

        return view('products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $product = $this->productInterface->update($request->produto, $request->name);

        return redirect()
            ->route('produtos.show', $product->sku)
            ->with('status.success', 'Produto Atualizado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->productInterface->destroy($request->produto);

        return redirect()
            ->route('produtos.index')
            ->with('status.success', 'Produto removido!');
    }
}
