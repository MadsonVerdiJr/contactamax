<?php

namespace App\Http\Controllers;

use App\Repositories\ProductRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Product Repository Interface
     *
     * @var App\Repositories\ProductRepositoryInterface
     */
    private $productInterface;

    /**
     * Construct class
     *
     * @param ProductRepositoryInterface $productInterface - Product Repository Interface
     */
    public function __construct(ProductRepositoryInterface $productInterface)
    {
        $this->middleware('auth');
        $this->productInterface = $productInterface;
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $carbon = new Carbon;

        if (isset($request->date)) {
            // Date format: d-m-y
            $currentDate = $carbon->createFromFormat('d-m-y', $request->date);
            // check if date is future
            if ($currentDate->isFuture()) {
                return redirect()->route('dashboard');
            }
        } else {
            $currentDate = $carbon->now();
        }

        $nextDay = $currentDate->copy()->addDay();
        $previousDay = $currentDate->copy()->subDay();

        $productsLowStock = $this->productInterface->getAllLowStock();
        $stockIn = $this->productInterface->getDailyStockIn($currentDate);
        $stockOut = $this->productInterface->getDailyStockOut($currentDate);

        return view('dashboard', compact(
            'currentDate',
            'nextDay',
            'previousDay',
            'productsLowStock',
            'stockIn',
            'stockOut'
        ));
    }
}
