<?php

namespace App\Http\Controllers;

use App\Http\Requests\ApiProductStockChangeRequest;
use App\Repositories\ProductRepositoryInterface;

class ApiProductStockRemoveController extends Controller
{
    /**
     * Product Repository Interface
     *
     * @var App\Repositories\ProductRepositoryInterface
     */
    private $productInterface;

    /**
     * Construct class
     *
     * @param ProductRepositoryInterface $productInterface - Product Repository Interface
     */
    public function __construct(ProductRepositoryInterface $productInterface)
    {
        $this->middleware('auth.basic');
        $this->productInterface = $productInterface;
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke(ApiProductStockChangeRequest $request)
    {
        $product = $this->productInterface->getBySku($request->sku);

        $this->productInterface->removeQuantity($product, 'API', $request->quantity);

        return response()->json(true);
    }
}
