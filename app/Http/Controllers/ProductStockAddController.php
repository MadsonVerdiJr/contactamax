<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddToStockRequest;
use App\Repositories\ProductRepositoryInterface;
use Illuminate\Http\Request;

class ProductStockAddController extends Controller
{
    /**
     * Product Repository Interface
     *
     * @var App\Repositories\ProductRepositoryInterface
     */
    private $productInterface;

    /**
     * Construct class
     *
     * @param ProductRepositoryInterface $productInterface - Product Repository Interface
     */
    public function __construct(ProductRepositoryInterface $productInterface)
    {
        $this->middleware('auth');
        $this->productInterface = $productInterface;
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke(AddToStockRequest $request)
    {
        $product = $this->productInterface->getBySku($request->produto);

        $this->productInterface->addQuantity($product, 'Sistema', $request->quantity);

        return redirect()
            ->back()
            ->with('status.success', 'Quantidade atualizada!');
    }
}
