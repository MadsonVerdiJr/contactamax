<?php

namespace App\Repositories;

/**
 * Product Repository Interface
 */
interface ProductRepositoryInterface
{
    /**
     * Add stock quantity for product
     *
     * @param integer $productId - ID of the product
     * @param string  $method    - Method used for creating ('Sistema' | 'API')
     * @param integer $amount    - Amount of quantity to add to stock
     *
     * @return  void
     */
    public function addQuantity(\App\Product $product, string $method, $amount) : void;

    /**
     * Destroy product
     *
     * @param  integer $identifier - ID of the product
     *
     * @return void
     */
    public function destroy($identifier);

    /**
     * Get all low Stock Products
     *
     * @return App\Product
     */
    public function getAllLowStock() : \Illuminate\Database\Eloquent\Collection;

    /**
     * Get all product Paginated
     *
     * @return Illuminate\Pagination\Paginator
     */
    public function getAllPaginated();

    /**
     * Get product by SKU
     *
     * @param  string $sku - SKU code
     *
     * @return App\Product
     */
    public function getBySku(string $sku);

    /**
     * Get stock daily stock log from added
     *
     * @param  \Carbon\Carbon $date - Date from report
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getDailyStockIn(\Carbon\Carbon $date) : \Illuminate\Database\Eloquent\Collection;

    /**
     * Get stock daily stock log from removed
     *
     * @param  \Carbon\Carbon $date - Date from report
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getDailyStockOut(\Carbon\Carbon $date) : \Illuminate\Database\Eloquent\Collection;

    /**
     * Remove stock quantity for product
     *
     * @param  \App\Product $product - Product object
     * @param  string       $method  - Method used for creating ('Sistema' | 'API')
     * @param  integer      amount   - Amount of quantity to remove from stock
     *
     * @return void
     */
    public function removeQuantity(\App\Product $product, string $method, $amount) : void;

    /**
     * Store a product
     *
     * @param  string   $name     - Name of the product
     * @param  integer  $quantity - Quantity of product in stock
     *
     * @return \App\Product
     */
    public function store(string $name, $quantity) : \App\Product;

    /**
     * Update a product
     *
     * @param  integer $identifier  - ID of the product
     * @param  string  $name        - Name of the product
     *
     * @return App\Product
     */
    public function update($identifier, string $name);
}
