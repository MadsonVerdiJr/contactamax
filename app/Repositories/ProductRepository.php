<?php

namespace App\Repositories;

use App\Product;
use App\ProductStock;

/**
 * Product Repository
 */
class ProductRepository implements ProductRepositoryInterface
{

    /**
     * Generate a SKU
     *
     * @param  integer $identifier - ID
     * @param  string $name        - Name
     *
     * @return string
     */
    private function generateSku($identifier, string $name) : string
    {
        return str_limit(strtoupper(str_slug($name)), 3, '') .
            str_pad($identifier, 6, '0', STR_PAD_LEFT);
    }

    /**
     * Add stock quantity for product
     *
     * @param integer $productId - ID of the product
     * @param string  $method    - Method used for creating ('Sistema' | 'API')
     * @param integer $amount    - Amount of quantity to add to stock
     *
     * @return  void
     */
    public function addQuantity(\App\Product $product, string $method, $amount) : void
    {
        $amount = abs($amount);

        $product->update([
            'quantity' => $product->quantity + $amount
        ]);

        $product->stock()->create([
            'quantity' => $amount,
            'type' => 'in',
            'method' => $method
        ]);
    }

    /**
     * Destroy product
     *
     * @param  integer $identifier - ID of the product
     *
     * @return bool|null
     */
    public function destroy($identifier)
    {
        $product = new Product;
        $product = $product->findOrFail($identifier);

        return $product->delete();
    }

    /**
     * Get all low Stock Products
     *
     * @return App\Product
     */
    public function getAllLowStock() : \Illuminate\Database\Eloquent\Collection
    {
        $product = new Product;

        return $product->lowStock()->get();
    }

    /**
     * Get all product Paginated
     *
     * @return Illuminate\Pagination\Paginator
     */
    public function getAllPaginated()
    {
        $product = new Product;

        return $product->paginate(15);
    }

    /**
     * Get stock daily stock log from added
     *
     * @param  \Carbon\Carbon $date - Date from report
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getDailyStockIn(\Carbon\Carbon $date) : \Illuminate\Database\Eloquent\Collection
    {
        $stock = new ProductStock;

        return $stock->typeIn()->daily($date)->get();
    }

    /**
     * Get stock daily stock log from removed
     *
     * @param  \Carbon\Carbon $date - Date from report
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getDailyStockOut(\Carbon\Carbon $date) : \Illuminate\Database\Eloquent\Collection
    {
        $stock = new ProductStock;

        return $stock->typeOut()->daily($date)->get();
    }
    /**
     * Get product by SKU
     *
     * @param  string $sku - SKU code
     *
     * @return App\Product
     */
    public function getBySku(string $sku)
    {
        $product = new Product;

        return $product->whereSku($sku)->first();
    }

    /**
     * Remove stock quantity for product
     *
     * @param  \App\Product $product - Product object
     * @param  string       $method  - Method used for creating ('Sistema' | 'API')
     * @param  integer      amount   - Amount of quantity to remove from stock
     *
     * @return void
     */
    public function removeQuantity(\App\Product $product, string $method, $amount) : void
    {
        // prevent negative values
        $amount = abs($amount);

        // only available itens are removed
        if ($product->quantity < $amount) {
            $amount = $product->quantity;
        }

        $product->update([
            'quantity' => $product->quantity - $amount
        ]);

        $product->stock()->create([
            'quantity' => $amount,
            'type' => 'out',
            'method' => $method
        ]);
    }

    /**
     * Store a product
     *
     * @param  string   $name     - Name of the product
     * @param  integer  $quantity - Quantity of product in stock
     *
     * @return \App\Product
     */
    public function store(string $name, $quantity) : \App\Product
    {
        $product = new Product;

        $product = $product->create([
            'name' => $name,
            'quantity' => $quantity
        ]);

        // Must create to use ID in SKU
        $product->update([
            'sku' => $this->generateSku($product->id, $product->name)
        ]);

        return $product;
    }


    /**
     * Update a product
     *
     * @param  integer $identifier  - ID of the product
     * @param  string  $name        - Name of the product
     *
     * @return App\Product
     */
    public function update($identifier, string $name)
    {
        $product = new Product;
        $product = $product->findOrFail($identifier);

        $product->update([
            'name' => $name,
            'sku' => $this->generateSku($identifier, $name)
        ]);

        return $product;
    }
}
