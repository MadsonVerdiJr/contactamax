<?php

namespace Tests\Unit;

use App;
use App\Product;
use App\Repositories\ProductRepositoryInterface;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Artisan;

class ProductUnitTest extends TestCase
{
    use RefreshDatabase;

    private $productInterface;

    public function setUp(){
        parent::setUp();
        $this->productInterface = App::make(ProductRepositoryInterface::class);
        $this->faker = \Faker\Factory::create();
    }

    /** Add to stock */
    public function testAddStock()
    {
        $name = 'Remove Stock test';

        // Create instance
        $product = $this->productInterface->store($name, 0);

        $this->assertInstanceOf(Product::class, $product);

        $this->assertEquals(0, $product->quantity);

        $this->productInterface->addQuantity($product, 'API', 100000);

        $this->assertEquals(100000, $product->quantity);

        $lastStock = $product->stock()->orderBy('created_at', 'asc')->first();

        $this->assertEquals(100000, $lastStock->quantity);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateProduct()
    {
        $name = 'test';

        // Create instance
        $product = $this->productInterface->store($name, 0);

        $this->assertInstanceOf(Product::class, $product);
        $this->assertEquals($name, $product->name);
        $this->assertStringStartsWith('TES', $product->sku);
    }

    /**
     * Destroy test
     *
     * @return void
     */
    public function testDestroyProduct()
    {
        $name = 'Show Name';

        // Create instance
        $product = $this->productInterface->store($name, 0);

        $destroy = $this->productInterface->destroy($product->id);

        $this->assertTrue($destroy);
    }

    public function testLowStockProducts()
    {
        $this->productInterface->store($this->faker->name, 100);
        $this->productInterface->store($this->faker->name, 101);
        $this->productInterface->store($this->faker->name, 90);
        $this->productInterface->store($this->faker->name, 0);
        $this->productInterface->store($this->faker->name, 1);

        $products = $this->productInterface->getAllLowStock();

        $this->assertEquals(3, $products->count());
    }

    /** Remove from stock */
    public function testRemoveStock()
    {
        $name = 'Remove Stock test';

        // Create instance
        $product = $this->productInterface->store($name, 0);

        $this->assertInstanceOf(Product::class, $product);

        $this->assertEquals(0, $product->quantity);

        $this->productInterface->removeQuantity($product, 'API', 10);

        $this->assertEquals(0, $product->quantity);
    }

    /**
     * Show test
     *
     * @return void
     */
    public function testShowProduct()
    {
        $name = 'Show Name';

        // Create instance
        $oldProduct = $this->productInterface->store($name, 0);


        // Get instance
        $product = $this->productInterface->getBySku($oldProduct->sku);

        $this->assertInstanceOf(Product::class, $product);

        $this->assertStringStartsWith('SHO', $product->sku);
        $this->assertEquals($oldProduct->name, $product->name);
        $this->assertEquals($oldProduct->sku, $product->sku);
        $this->assertEquals($oldProduct->id, $product->id);
    }

    /**
     * Update test
     *
     * @return void
     */
    public function testUpdateProduct()
    {
        // Create instance
        $oldProduct = $this->productInterface->store('apenas um teste', 0);

        $name = 'newName';

        // Update instance
        $product = $this->productInterface->update($oldProduct->id, $name);

        $this->assertInstanceOf(Product::class, $product);
        $this->assertNotEquals($oldProduct->name, $product->name);
        $this->assertEquals($name, $product->name);
        $this->assertStringStartsWith('NEW', $product->sku);
    }

}
