<?php

// Home
Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push('Home', route('home'));
});

Breadcrumbs::register('dashboard', function ($breadcrumbs) {
    $breadcrumbs->push('Dashboard', route('dashboard'));
});

// Produtos > index
Breadcrumbs::register('produtos.index', function($breadcrumbs) {
    $breadcrumbs->push('Produtos', route('produtos.index'));
});

// Produtos > Criar
Breadcrumbs::for('produtos.create', function ($trail) {
    $trail->parent('produtos.index');
    $trail->push('Criar Produto', route('produtos.create'));
});

// Produtos > [SKU]
Breadcrumbs::for('produtos.show', function ($trail, $product) {
    $trail->parent('produtos.index');
    $trail->push($product->sku, route('produtos.show', $product->sku));
});

// Produtos > [SKU] > Edit
Breadcrumbs::for('produtos.edit', function ($trail, $product) {
    $trail->parent('produtos.show', $product);
    $trail->push('Editar', route('produtos.edit', $product->sku));
});