@extends('layout')

@section('breadc')
    {{ Breadcrumbs::render('produtos.create') }}
@endsection

@section('content')

    {{ Form::open(['method' => 'POST', 'route' => 'produtos.store']) }}

        {{ Form::bsText('name', 'Nome do produto') }}

        {{ Form::bsNumber('quantity', 'Quantidade de produtos em estoque', 0) }}

        {{ Form::bsSubmit('Criar') }}

    {{ Form::close() }}

@endsection