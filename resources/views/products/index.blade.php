@extends('layout')

@section('breadc')
    {{ Breadcrumbs::render('produtos.index') }}
@endsection

@section('content')

{{-- Ação para adicionar novo produto --}}
<a href="{{ route('produtos.create') }}" class="btn btn-default">Criar produto</a>

{{-- Tabela com produtos cadastrados --}}
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>SKU</th>
            <th>Nome</th>
            <th>Quantidade em estoque</th>
            <th>Ações</th>
        </tr>
    </thead>
    <tbody>
        @forelse($products as $product)
        <tr>
            <td>{{ $product->sku }}</td>
            <td>{{ $product->name }}</td>
            <td>{{ $product->quantity }}</td>
            <td>
                <ul>
                    <li>
                        <a href="{{ route('produtos.show', $product->sku) }}">Visualizar</a>
                    </li>
                    <li>
                        <a href="#" data-toggle="modal" data-target="#productStockAddModal" data-action="{{ route('produtos.add-stock', $product->sku) }}">Adicionar ao estoque</a>
                    </li>
                    <li>
                        <a href="#" data-toggle="modal" data-target="#productStockRemoveModal" data-action="{{ route('produtos.remove-stock', $product->sku) }}">Remover do estoque</a>
                    </li>
                </ul>
            </td>
        </tr>
        @empty
        <tr>
            <td colspan="4">Nenhum produto cadastrado</td>
        </tr>
        @endforelse
    </tbody>
    <tfoot>
        <tr>
            <td colspan="4">{{ $products->links() }}</td>
        </tr>
    </tfoot>
</table>

@endsection

@include('components.modal.productStockAdd')
@include('components.modal.productStockRemove')