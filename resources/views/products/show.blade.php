@extends('layout')

@section('breadc')
    {{ Breadcrumbs::render('produtos.show', $product) }}
@endsection

@section('content')

<div class="card">
    <div class="card-body">
        <h5 class="card-title">{{ $product->name }}</h5>
        <h6 class="card-subtitle mb-2 text-muted">{{ $product->sku }}</h6>

        <p class="card-text">
            Quantidade de itens em estoque: {{ $product->quantity }}.
            <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
              <button type="button" data-toggle="modal" data-target="#productStockAddModal" data-action="{{ route('produtos.add-stock', $product->sku) }}" class="btn btn-primary">Adicionar ao estoque</button>
              <button type="button" data-toggle="modal" data-target="#productStockRemoveModal" data-action="{{ route('produtos.remove-stock', $product->sku) }}" class="btn btn-light">Remover do estoque</button>
            </div>
        </p>

        <a href="{{ route('produtos.edit', $product->sku) }}" class="card-link">Editar</a>
  </div>
</div>

@endsection

@include('components.modal.productStockAdd')
@include('components.modal.productStockRemove')