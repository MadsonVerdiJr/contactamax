@extends('layout')

@section('breadc')
    {{ Breadcrumbs::render('produtos.edit', $product) }}
@endsection

@section('content')

    {{ Form::open(['method' => 'PUT', 'route' => ['produtos.update', $product->id]]) }}

        {{ Form::bsText('name', 'Nome do produto', old('name', $product->name)) }}

        {{ Form::bsSubmit('Editar') }}

        <a href="#" class="text-secondary" data-toggle="modal" data-target="#productDestroy">Remover</a>

    {{ Form::close() }}

@endsection

@include('components.modal.destroy', [
    'action' => route('produtos.destroy', $product->id),
    'modal_name' => 'productDestroy'
])