
@push('modal')

  <!-- Modal -->
  <div class="modal fade" id="productStockAddModal">
    {{ Form::open(['url' => '', 'method' => 'POST']) }}
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Adicionar ao estoque?</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            {{ Form::bsNumber('quantity', 'Quandidade de itens') }}
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
              {{ Form::submit('Adicionar', ['class' => 'btn btn-primary']) }}
          </div>
        </div>
      </div>
    {{ Form::close() }}
  </div>

@endpush


@push('js-helpers')
<script>
  $('#productStockAddModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var recipient = button.data('action') // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this)
    console.log(modal)
    modal.find('form').prop('action', recipient)
  })
</script>
@endpush