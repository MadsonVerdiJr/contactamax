
@push('modal')

  <!-- Modal -->
  <div class="modal fade" id="{{ camel_case($modal_name) }}">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="{{ camel_case($modal_name) }}Title">Remover?</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Esta ação não poderá ser revertida <span class="text-danger"><span class="oi oi-warning"></span></span>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>

          {{ Form::open(['url' => $action, 'method' => 'DELETE']) }}
            {{ Form::submit('Confirmar', ['class' => 'btn btn-danger']) }}
          {{ Form::close() }}
        </div>
      </div>
    </div>
  </div>

@endpush
