<div class="form-group">
    {{ Form::label($name, $label, ['class' => 'form-control-label']) }}
    {{ Form::textarea($name, $value, array_merge(['class' => $errors->has($name) ? 'form-control is-invalid' : 'form-control', 'rows' => '5'], $attributes)) }}
    {!! $errors->first($name, '<p class="invalid-feedback">:message</p>')  !!}
</div>
