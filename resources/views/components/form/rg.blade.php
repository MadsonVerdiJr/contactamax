<div class="form-group">
    {{ Form::label($name, $label, ['class' => 'form-control-label']) }}
    {{ Form::text($name, $value, array_merge(['class' => $errors->has($name) ? 'form-control is-invalid' : 'form-control'], $attributes)) }}
    {!! $errors->first($name, '<p class="invalid-feedback">:message</p>')  !!}
</div>

@push('js-helpers')
    <script>
        $('#{{ $name }}').mask('################')
    </script>
@endpush
