<div class="container">
    @if (session('status.success'))
      <div class="alert alert-success">
        {{ session('status.success') }}
      </div>
    @endif

    @yield('content')
</div>