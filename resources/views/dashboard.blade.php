@extends('layout')

@section('breadc')
    {{ Breadcrumbs::render('dashboard') }}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">{{ $currentDate->format('d/m/y') }}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">Relatórios diários</h6>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm">
                            <h5 class="text-muted">Produtos adicionados ao estoque</h5>
                            @include('stock.table-daily-compact', ['stocks' => $stockIn])
                        </div>
                        <div class="col-sm">
                            <h5 class="text-muted">Produtos removidos do estoque</h5>
                            @include('stock.table-daily-compact', ['stocks' => $stockOut])
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <a href="{{ route('dashboard', $previousDay->format('d-m-y')) }}" class="card-link">Dia anterior</a>
                    @if (!$currentDate->isToday())
                        <a href="{{ route('dashboard', $nextDay->format('d-m-y')) }}" class="card-link">Próximo Dia</a>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-header">
                    <h5>Alerta de poucas unidades</h5>
                </div>

                @include('stock.alert-table-compact', ['products' => $productsLowStock])
            </div>
        </div>
    </div>
@endsection