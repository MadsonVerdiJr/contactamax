<div class="list-group">
    @forelse($stocks as $stock)
    <a href="{{ route('produtos.show', $stock->product->sku) }}" class="list-group-item list-group-item-action flex-column align-items-start">
        <div class="d-flex w-100 justify-content-between">
          <h5 class="mb-1">{{ $stock->product->name }}</h5>
          <small>{{ $stock->method }}</small>
        </div>
        <p class="mb-1">
            {{ $stock->product->sku }}
        </p>
        <small class="text-{{ $stock->type == 'in' ? 'success' : 'danger' }}">Quantidade: {{ $stock->quantity }}</small>
    </a>
    @empty
    <div class="list-group-item">
        Nenhum produto cadastrado neste dia
    </div>
    @endforelse
</div>