{{-- Tabela com produtos cadastrados --}}

<div class="list-group">
    @forelse($products as $product)
    <a href="{{ route('produtos.show', $product->sku) }}" class="list-group-item list-group-item-action">
        {{ $product->sku }}
        <span class="badge badge-danger badge-pill">
            {{ $product->quantity }}
        </span>
    </a>
    @empty
    <div class="list-group-item">
        Nenhum alerta.
    </div>
    @endforelse
</div>
