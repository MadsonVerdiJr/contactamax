<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}  @yield('title')</title>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  </head>
  <body>

    {{-- Header --}}
    @component('components.layout.header')
    @endcomponent

    {{-- Content --}}
    @component('components.layout.page-content')
    @endcomponent

    {{-- Footer --}}
    @component('components.layout.footer')
    @endcomponent

  </body>
</html>
